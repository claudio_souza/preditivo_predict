from google.cloud import bigquery
from src.config.config import GCLOUD_CREDENTIALS_FILEPATH
from src.etl.etl_utils import procedimentos_by_model, query_fenix, get_keys_from_dict_as_list


class ETL:
    """Classe responsável por todos os processos de ETL do dataset que será passado para o modelo preditivo."""

    def __init__(self, model_name, ref_date, id_empresa):
        self.model_name = model_name
        self.ref_date = ref_date
        self.id_empresa = id_empresa

    def extract(self):
        print("Inicializando cliente do BigQuery.")
        client = bigquery.Client.from_service_account_json(json_credentials_path=GCLOUD_CREDENTIALS_FILEPATH)

        print(f"Extraindo dados do BigQuery.")
        query = query_fenix.substitute(
            ref_date=self.ref_date,
            id_empresa=self.id_empresa
        )
        extracted_df = client.query(query).to_dataframe(progress_bar_type='tqdm')

        return extracted_df

    @staticmethod
    def transform(extracted_df, model_name):
        """Método responsável pela transformação de um dataframe "raw" em um dataframe aceito pelo modelo.

         O método consiste de 3 passos:
            1. Agrupa os dados do `extracted_df` por pessoa.
            2. categoriza os procedimentos selecionados de acordo com o `hash_procedimentos_descricao`.
            3. contabiliza a quantidade de procedimentos realizados em cada categoria.

        Params:
            extracted_df: dataset em formato raw.

        Returns:
            transformed_df: dataset em formato clean.
        """

        transformed_df = extracted_df.copy()

        # Mapeamento id -> descrição de procedimentos.
        transformed_df['idProcedimento'] = transformed_df['idProcedimento']\
            .map(procedimentos_by_model[model_name])\
            .fillna('outros')

        # Contagem de procedimentos selecionados por pessoa.
        transformed_df = transformed_df.groupby(['idPessoa', 'idProcedimento'])\
            .agg(count=('idProcedimento', 'count'))

        # Formatação do dataframe resultante para ficar no padrão definido.
        transformed_df = transformed_df.reset_index()\
            .pivot(index='idPessoa', columns='idProcedimento', values='count')\
            .rename_axis('', axis='columns')\
            .drop(columns=['outros'], errors='ignore')\
            .fillna(0)

        return transformed_df

    def run(self):
        # Extract.
        extracted_df = self.extract()

        # Transform.
        transformed_df = self.transform(extracted_df, self.model_name)

        return transformed_df
