from google.cloud import storage
from src.config.config import GCLOUD_CREDENTIALS_FILEPATH


def upload_file(bucket_name, destination_blob_name, source_file_path):
    """Uploads a file to the bucket."""

    storage_client = storage.Client.from_service_account_json(json_credentials_path=GCLOUD_CREDENTIALS_FILEPATH)
    bucket = storage_client.bucket(bucket_name)

    blob = bucket.blob(destination_blob_name)
    blob.upload_from_filename(source_file_path)

    print(f"File uploaded to {destination_blob_name}.")


def download_blob(bucket_name, source_blob_name, destination_file_name):
    """Downloads a blob from the bucket."""

    storage_client = storage.Client.from_service_account_json(json_credentials_path=GCLOUD_CREDENTIALS_FILEPATH)
    bucket = storage_client.bucket(bucket_name)

    blob = bucket.blob(source_blob_name)
    blob.download_to_filename(destination_file_name)

    print(f"Blob {source_blob_name} downloaded to {destination_file_name}.")
