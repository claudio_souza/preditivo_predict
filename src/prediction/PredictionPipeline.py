import os
import pandas as pd
from joblib import load
from datetime import datetime
from google.cloud import bigquery
from src.utils.gcp_utils import download_blob
from src.config.config import GCLOUD_CREDENTIALS_FILEPATH, PREDICT_RESULTS_TABLE
from src.config.config import PREDITIVOS_FOLDER_NAME, MODEL_NAME, GCLOUD_BUCKET, MODELS_DIR


def calc_risk(probability, threshold=0.5):
    if probability < threshold:
        return 'baixo'
    else:
        return 'alto'


def df_to_bigquery(df, table_id, credentials_json):
    print('Iniciando salvamento dos dados do predict no BigQuery.')
    client = bigquery.Client.from_service_account_json(credentials_json)
    client.load_table_from_dataframe(df, table_id).result()


def feature_selection(features, df):
    # Muitas vezes, as features que foram selecionadas no treino podem não existir no momento do predict,
    # e vice-versa. Nesses casos, vamos setar os dados das colunas faltantes como 0.
    print('Iniciando seleção de features.')

    intersection = [col for col in features if col in df.columns]
    intersection_ratio = round(100 * len(intersection) / len(features), 2)
    print(f'{intersection_ratio}% das features selecionadas no treino existem no dataset atual.')

    # Inputar valores de colunas faltantes.
    selected_X = df.copy()
    for col in features:
        if col not in selected_X.columns:
            selected_X[col] = 0

    # Dropar colunas que não serão usadas no modelo e reordenar colunas pra ficar na mesma ordem do treinamento.
    selected_X = selected_X[features]
    return selected_X


class PredictionPipeline:
    def __init__(self, model_name, id_empresa, ref_date):
        self.model_name = model_name
        self.id_empresa = id_empresa
        self.ref_date = ref_date

    def load_model(self):
        # Carregar modelo e features do bucket.
        print('Carregando modelo e features do bucket no Google Cloud Storage.')
        LOCAL_MODEL_FILEPATH = f'{MODELS_DIR}/{self.model_name}/{MODEL_NAME}'
        os.makedirs(os.path.dirname(LOCAL_MODEL_FILEPATH), exist_ok=True)

        SOURCE_FILEPATH = f'{PREDITIVOS_FOLDER_NAME}/{self.model_name}/{MODEL_NAME}'
        print(f'Fazendo download do modelo {SOURCE_FILEPATH}/{self.model_name} em {LOCAL_MODEL_FILEPATH}.')
        download_blob(
            bucket_name=GCLOUD_BUCKET,
            source_blob_name=SOURCE_FILEPATH,
            destination_file_name=LOCAL_MODEL_FILEPATH
        )

        model_struct = load(LOCAL_MODEL_FILEPATH)
        clf = model_struct['model']
        features = model_struct['features']

        os.remove(LOCAL_MODEL_FILEPATH)

        return clf, features

    def run(self, in_df):
        if len(in_df) == 0:
            print('Os parâmetros selecionados não retornaram dados. Finalizando pipeline de predict.')
            exit()

        # Carregar modelo e features do bucket.
        clf, features = self.load_model()

        # Preparar dados para predict.
        X = in_df.copy()
        print(f'Tamanho do dataset de predict: {len(X)} amostras.')

        # Executar seleção de features.
        selected_X = feature_selection(features, X)

        # Fazer predict.
        print('\tIniciando predict.')
        predictions = clf.predict_proba(selected_X)
        positive_predictions = predictions[:, 1]

        # Preparar dataframe para salvar os resultados do predict.
        output_df = pd.DataFrame(
            data=positive_predictions,
            index=selected_X.index,
            columns=['probabilidade_predita']
        )
        output_df['risco'] = output_df['probabilidade_predita'].apply(calc_risk)
        output_df['TipoPredicao'] = self.model_name
        output_df['IdEmpresa'] = int(self.id_empresa)
        output_df['AnoCompetencia'] = pd.to_datetime(self.ref_date).year
        output_df['MesCompetencia'] = pd.to_datetime(self.ref_date).month
        output_df['IdPessoa'] = output_df.index.values
        output_df['timestamp'] = pd.to_datetime(datetime.now())
        output_df = output_df.reset_index(drop=True)
        output_df = output_df[["IdEmpresa", "AnoCompetencia", "MesCompetencia",
                               "IdPessoa", "probabilidade_predita", "risco", "TipoPredicao", "timestamp"]]

        # Salvar resultados do predict no BigQuery.
        df_to_bigquery(output_df, PREDICT_RESULTS_TABLE, GCLOUD_CREDENTIALS_FILEPATH)
