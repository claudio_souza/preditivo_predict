import os

CONFIG_DIR = os.path.dirname(os.path.abspath(__file__))
SRC_DIR = os.path.dirname(CONFIG_DIR)
ROOT_DIR = os.path.dirname(SRC_DIR)

# Runtime environment.
RUNTIME_ENVIRONMENT = os.getenv('RUNTIME_ENVIRONMENT')

# Credenciais de acesso ao GCP.
BIGQUERY_DATASET_NAME = 'fenix_bi'

if RUNTIME_ENVIRONMENT == 'dev':
    GCLOUD_PROJECT = 'laboratorio-eng-dados'
    GCLOUD_BUCKET = 'dev-ml-model'
    GCLOUD_CREDENTIALS_FILEPATH = f'{CONFIG_DIR}/gcloud_credentials_dev.json'
    ETL_INPUT_TABLE = f"{GCLOUD_PROJECT}.{BIGQUERY_DATASET_NAME}.view_fato_sinistro_without_cte"
else:
    GCLOUD_PROJECT = 'cosmic-shift-235317'
    GCLOUD_BUCKET = 'prd-ml-models'
    GCLOUD_CREDENTIALS_FILEPATH = f'{CONFIG_DIR}/gcloud_credentials_prod.json'
    ETL_INPUT_TABLE = f"{GCLOUD_PROJECT}.{BIGQUERY_DATASET_NAME}.view_fato_sinistro_beta"

PREDICT_RESULTS_TABLE = f"{GCLOUD_PROJECT}.{BIGQUERY_DATASET_NAME}.fato_preditivo_new"

# Variáveis relacionadas ao GCP Storage.
PREDITIVOS_FOLDER_NAME = 'preditivos'
MODEL_NAME = 'model_struct.joblib'

# Caminhos para as pastas de artefatos.
ARTIFACT_DIR = f'{ROOT_DIR}/artifacts'
MODELS_DIR = f'{ARTIFACT_DIR}/models'
