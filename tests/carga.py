import requests
import json

url_predict = 'https://prd-preditivo-predict-ofulylkqja-ue.a.run.app'
# url_predict = ' https://dev-preditivo-predict-zm3msiaf7q-ue.a.run.app'

model_names = ['bariatrica', 'neonatal', 'ortopedia', 'bucomaxilo', 'cardio']

calls = [
    (159, "2021-03"), (163, "2021-03"), (218, "2021-03"), (223, "2021-03"), (235, "2021-03"),
    (242, "2021-03"), (243, "2021-03"), (244, "2021-03"), (247, "2021-03"), (253, "2021-03"),
    (259, "2021-04"), (263, "2021-03"), (264, "2021-03"), (266, "2021-04"), (267, "2021-03"),
    (268, "2021-03"), (269, "2021-02"), (270, "2021-03"), (271, "2021-03"), (272, "2021-03"),
    (273, "2021-03"), (274, "2021-04"), (275, "2021-03"), (276, "2021-03"), (277, "2021-01"),
    (278, "2021-02"), (279, "2020-12"), (280, "2021-03"), (281, "2021-02"), (282, "2021-03"),
    (283, "2020-12"), (284, "2021-03"), (285, "2021-03"), (287, "2021-01"), (288, "2021-01"),
    (289, "2021-03"), (290, "2021-04")
]


def post_request(url, payload):
    response = requests.post(url, data=json.dumps(payload))
    print(response.text)


if __name__ == '__main__':
    for call in calls:
        id_empresa, ref_date = call

        for model_name in model_names:
            payload = {"model": model_name, "ref_date": ref_date, "id_empresa": id_empresa}
            print(payload)
            post_request(url_predict, payload)
